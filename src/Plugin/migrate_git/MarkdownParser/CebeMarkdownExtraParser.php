<?php

namespace Drupal\migrate_git\Plugin\migrate_git\MarkdownParser;

use cebe\markdown\MarkdownExtra;
use Drupal\migrate_git\MarkdownParserPluginInterface;

/**
 * Convert Markdown to HTML with the Cebe Markdown Extra Parser.
 *
 * PHP Library cebe/markdown ^1.2 must be installed.
 *
 * @MarkdownParser(
 *   id = "cebe_markdown_extra",
 *   title = @Translation("Cebe Markdown Extra Parser")
 * )
 */
class CebeMarkdownExtraParser implements MarkdownParserPluginInterface {

  /**
   * A markdown parser to parse includes.
   *
   * @var \cebe\markdown\MarkdownExtra
   */
  protected $parser;

  /**
   * Constructs a Cebe Markdown Parser Object.
   */
  public function __construct() {
    $this->parser = new MarkdownExtra();
    $this->parser->html5 = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function parse($input) {
    return $this->parser->parse($input);
  }

}
