<?php

namespace Drupal\migrate_git\Plugin\migrate_git\MarkdownParser;

use Drupal\migrate_git\MarkdownParserPluginInterface;

/**
 * Convert Markdown to HTML with the Parsedown Parser.
 *
 * @MarkdownParser(
 *   id = "parsedown",
 *   title = @Translation("Parsedown")
 * )
 */
class ParsedownParser implements MarkdownParserPluginInterface {

  /**
   * A markdown parser to parse includes.
   *
   * @var \Parsedown
   */
  protected $parser;

  /**
   * Constructs a parsedown object.
   */
  public function __construct() {
    $this->parser = new \Parsedown();
  }

  /**
   * {@inheritdoc}
   */
  public function parse($input) {
    return $this->parser->parse($input);
  }

}
