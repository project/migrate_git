<?php

namespace Drupal\migrate_git\Plugin\migrate_git\MarkdownParser;

use Drupal\migrate_git\MarkdownParserPluginInterface;
use League\CommonMark\CommonMarkConverter;

/**
 * Convert Markdown to HTML with the League Common Mark Parser.
 *
 * PHP Library league/commonmark ^1.0 must be installed.
 *
 * @MarkdownParser(
 *   id = "commonmark",
 *   title = @Translation("League CommonMark")
 * )
 */
class CommonMarkParser implements MarkdownParserPluginInterface {

  /**
   * A markdown parser to parse includes.
   *
   * @var \League\CommonMark\CommonMarkConverter
   */
  protected $parser;

  /**
   * Constructs a League CommonMarkParser object.
   */
  public function __construct() {
    $this->parser = new CommonMarkConverter();
  }

  /**
   * {@inheritdoc}
   */
  public function parse($input) {
    return $this->parser->convertToHtml($input);
  }

}
