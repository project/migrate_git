<?php

namespace Drupal\migrate_git\Plugin\migrate_git\MarkdownParser;

use cebe\markdown\GithubMarkdown;
use Drupal\migrate_git\MarkdownParserPluginInterface;

/**
 * Convert Markdown to HTML with the Cebe Markdown Parser for Github.
 *
 * PHP Library cebe/markdown ^1.2 must be installed.
 *
 * @MarkdownParser(
 *   id = "cebe_github",
 *   title = @Translation("Cebe Markdown Parser - Github Flavoured")
 * )
 */
class CebeMarkdownGithubParser implements MarkdownParserPluginInterface {

  /**
   * A markdown parser to parse includes.
   *
   * @var \cebe\markdown\GithubMarkdown
   */
  protected $parser;

  /**
   * Constructs a Cebe Markdown Parser Object.
   */
  public function __construct() {
    $this->parser = new GithubMarkdown();
    $this->parser->html5 = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function parse($input) {
    return $this->parser->parse($input);
  }

}
