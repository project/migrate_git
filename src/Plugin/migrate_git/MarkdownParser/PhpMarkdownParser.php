<?php

namespace Drupal\migrate_git\Plugin\migrate_git\MarkdownParser;

use Drupal\migrate_git\MarkdownParserPluginInterface;
use Michelf\Markdown;

/**
 * Convert Markdown to HTML with the michelf/php-markdown Parser.
 *
 * PHP Library michelf/php-markdown ^1.8 must be installed.
 *
 * @MarkdownParser(
 *   id = "php_markdown",
 *   title = @Translation("Php Markdown")
 * )
 */
class PhpMarkdownParser implements MarkdownParserPluginInterface {

  /**
   * A markdown parser to parse includes.
   *
   * @var \Michelf\Markdown
   */
  protected $parser;

  /**
   * Constructs a parsedown object.
   */
  public function __construct() {
    $this->parser = new Markdown();
  }

  /**
   * {@inheritdoc}
   */
  public function parse($input) {
    return $this->parser->transform($input);
  }

}
