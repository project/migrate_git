<?php

namespace Drupal\migrate_git\Plugin\migrate_git\MarkdownParser;

use Drupal\migrate_git\MarkdownParserPluginInterface;
use Michelf\MarkdownExtra;

/**
 * Convert Markdown to HTML with the michelf/php-markdown Markdown Extra Parser.
 *
 * PHP Library michelf/php-markdown ^1.8 must be installed.
 *
 * @MarkdownParser(
 *   id = "php_markdown_extra",
 *   title = @Translation("Php Markdown Extra")
 * )
 */
class PhpMarkdownExtraParser implements MarkdownParserPluginInterface {

  /**
   * A markdown parser to parse includes.
   *
   * @var \Michelf\MarkdownExtra
   */
  protected $parser;

  /**
   * Constructs a parsedown object.
   */
  public function __construct() {
    $this->parser = new MarkdownExtra();
  }

  /**
   * {@inheritdoc}
   */
  public function parse($input) {
    return $this->parser->transform($input);
  }

}
