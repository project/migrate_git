<?php

namespace Drupal\migrate_git\Plugin\migrate_git\MarkdownParser;

use cebe\markdown\Markdown;
use Drupal\migrate_git\MarkdownParserPluginInterface;

/**
 * Convert Markdown to HTML with the Cebe Markdown Parser for Github.
 *
 * PHP Library cebe/markdown ^1.2 must be installed.
 *
 * @MarkdownParser(
 *   id = "cebe_markdown",
 *   title = @Translation("Cebe Markdown Parser")
 * )
 */
class CebeMarkdownParser implements MarkdownParserPluginInterface {

  /**
   * A markdown parser to parse includes.
   *
   * @var \cebe\markdown\Markdown
   */
  protected $parser;

  /**
   * Constructs a Cebe Markdown Parser Object.
   */
  public function __construct() {
    $this->parser = new Markdown();
    $this->parser->html5 = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function parse($input) {
    return $this->parser->parse($input);
  }

}
