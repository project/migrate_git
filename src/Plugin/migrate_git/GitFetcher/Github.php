<?php

namespace Drupal\migrate_git\Plugin\migrate_git\GitFetcher;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate_git\GitFetcherPluginBase;
use Github\Client;
use Github\Exception\RuntimeException;

/**
 * Retrieve repositories from github via API.
 *
 * Available configuration keys:
 * - credentials/github_token: (optional) token to authenticate with the github
 *   api. Recommended for any but the simplest migrations to avoid api limits.
 * - limit_extensions: (optional) provide a list of file extensions to limit
 *   returned results to.
 * - include_dirs: (optional) If set, include directories in the rows passed to
 *   the parser plugin.
 * - recursive: (optional) default TRUE, if set walk provided directory(ies)
 *    recursively.
 *
 * For this git_fetcher plugin, the required structure of the repo array is
 * GithubNamespace/GitHubRepository:folder. To select whole folder, you can use
 * `/` for the folder value.
 *
 * Example:
 *
 * @code
 * source:
 *   plugin: git
 *   fetcher_plugin: github
 *   parser_plugin: file
 *   repos:
 *     - 'KnpLabs/php-github-api:doc'
 *     - 'php-http/guzzle6-adapter:tests'
 *     - 'php-http/guzzle6-adapter:src/Exception'
 *   recursive: true
 *   include_dirs: false
 *   limit_extensions:
 *     - md
 *     - txt
 *   credentials:
 *     github_token: TOKEN
 * @endcode
 *
 * Fields
 *   - name: example.png
 *   - path: images/example.png,
 *   - sha: f6b550816bb9ba1ef9d2639b65de5bdcb83b0057,
 *   - size: 296203,
 *   - url: https://api.github.com/repos/NAMESPACE/PROJECT/contents/images/example.png?ref=master',
 *   - html_url: 'https://github.com/NAMESPACE/PROJECT/blob/master/images/example.png',
 *   - git_url: 'https://api.github.com/repos/NAMESPACE/PROJECT/git/blobs/f6b550816bb9ba1ef9d2639b65de5bdcb83b0057',
 *   - download_url: 'https://raw.githubusercontent.com/NAMESPACE/PROJECT/master/images/example.png',
 *   - type: 'file',
 *   - _links':
 *     - self: 'https://api.github.com/repos/NAMESPACE/PROJECT/contents/images/example.png?ref=master',
 *     - git: 'https://api.github.com/repos/NAMESPACE/PROJECT/git/blobs/f6b550816bb9ba1ef9d2639b65de5bdcb83b0057',
 *     - html: 'https://github.com/NAMESPACE/PROJECT/blob/master/images/example.png',
 *
 * @GitFetcher(
 *   id = "github",
 *   title = @Translation("Github")
 * )
 */
class Github extends GitFetcherPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Github API client.
   *
   * @var \Github\Client
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = new Client();
    if (!isset($this->configuration['recursive'])) {
      $this->configuration['recursive'] = TRUE;
    }
    if (isset($configuration['credentials'])) {
      if (isset($configuration['credentials']['github_token'])) {
        $this->client->authenticate($configuration['credentials']['github_token'], NULL, Client::AUTH_HTTP_TOKEN);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getResponse($repo) {
    list($this->repo, $path) = explode(':', $repo);
    list($this->user, $this->repo) = explode('/', $this->repo);
    return $this->listFiles($path, $this->configuration['recursive']);
  }

  /**
   * {@inheritdoc}
   */
  public function listFiles($path, $recursive = TRUE) {
    try {
      $listing = $this->client->repo()->contents()->show($this->user, $this->repo, $path);
      if ($recursive) {
        foreach ($listing as $entry) {
          if ($entry['type'] === 'dir') {
            $listing = array_merge($listing, $this->listFiles($entry['path']));
          }
        }
      }
    }
    catch (RuntimeException $e) {
      throw new MigrateException('Error message: ' . $e->getMessage() . ' for ' . $path . 'in ' . $this->repo . '.');
    }
    if (empty($this->configuration['include_dirs'])) {
      $listing = array_filter($listing, function ($entry) {
        return !is_array($entry) || empty($entry['type']) || $entry['type'] !== 'dir';
      });
    }
    if (!empty($this->configuration['limit_extensions'])) {
      $listing = array_filter($listing, function ($entry) {
        return !is_array($entry) || empty($entry['type']) || in_array(
          pathinfo($entry['name'], PATHINFO_EXTENSION),
          $this->configuration['limit_extensions']
        );
      });
    }
    return $listing;
  }

  /**
   * Download file contents from a specific file in the git repository.
   *
   * @param string $path
   *   The path to the file to download.
   * @param array $row
   *   The full source row.
   *
   * @return string
   *   Contents of the file.
   *
   * @throws \Github\Exception\ErrorException
   */
  public function downloadFile($path, array $row) {
    if ($row['size'] < 1000000) {
      return $this->client->repo()
        ->contents()
        ->download($this->user, $this->repo, $path);
    }
    $blob = $this->client->gitData()->blobs()->show(
      $this->user,
      $this->repo,
      $row['sha']
    );
    return base64_decode($blob['content']);
  }

}
