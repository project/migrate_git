<?php

namespace Drupal\migrate_git\Plugin\migrate_git\GitParser;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate_git\GitFetcherPluginManager;
use Drupal\migrate_git\MarkdownParserPluginManager;
use Mni\FrontYAML\Parser;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Obtain file contents for migration.
 *
 * Enforces pre_download configuration to TRUE.
 *
 * Available configuration keys:
 *   - markdown_parser: (optional) Which Markdown Parser plugin to use. Defaults
 *     to parsedown. Note: other parsers require extra PHP libraries.
 *   - filter_by_yml: (optional) Provide a list of front matter keys to filter
 *     against. 'NULL' or '!NULL' can be provided as a value to match any/none
 *     instead of a specific value.
 *   - throw_error_on_missing_ids: (optional) Throw an hard error if row is
 *     missing an id. Most cases should have this or skip_missing_ids TRUE.
 *   - skip_missing_ids: (optional) Skip rows with missing IDs, recommended.
 *
 * Fields:
 *   - yml: Array of front matter yml.
 *   - processed_content: markdown parsed to HTML.
 *
 * Example:
 *
 * @code
 * source:
 *   plugin: git
 *   fetcher_plugin: github
 *   parser_plugin: frontyml
 *   repos:
 *     - 'earthlab/earthlab.github.io:_posts/courses'
 *   filter_by_yml:
 *     class-lesson: '!NULL'
 *   skip_missing_ids: true
 *   ids:
 *     'yml/class-lesson':
 *       type: string
 *     'yml/order':
 *       type: string
 *   credentials:
       github_token: 0df661808aa4d8ab72f9352a87768bbeb8db3875
 *   markdown_parser: php_markdown_extra
 * @endcode
 *
 * @GitParser(
 *   id = "frontyml",
 *   title = @Translation("FrontYml")
 * )
 */
class GitFrontYml extends GitFile implements ContainerFactoryPluginInterface {

  /**
   * The frontYAML Parser.
   *
   * @var \Mni\FrontYAML\Parser
   */
  protected $frontYmlParser;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GitFetcherPluginManager $fetcher_plugin_manager, MarkdownParserPluginManager $markdown_plugin_manager) {
    $configuration['pre_download'] = TRUE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $fetcher_plugin_manager);
    $parser = $this->configuration['markdown_parser'] ?? 'parsedown';
    /** @var \Drupal\migrate_git\MarkdownParserPluginInterface $markdown_parser */
    $markdown_parser = $markdown_plugin_manager->createInstance($parser, $this->configuration);
    $this->frontYmlParser = new Parser(NULL, $markdown_parser);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.migrate_git.git_fetcher'),
      $container->get('plugin.manager.migrate_git.markdown_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function preprocessRow(array &$row) {
    if (!parent::preprocessRow($row)) {
      return FALSE;
    }

    /*$content = preg_replace('/<i .*?></i>/', '', $row['content']);*/
    $data = $this->frontYmlParser->parse($row['content']);
    $row['yml'] = $data->getYAML();

    // Perform minimal pre-filtering via the filter_by_yml key.
    if (isset($this->configuration['filter_by_yml'])) {
      foreach ($this->configuration['filter_by_yml'] as $key => $value) {
        if (strpos($key, '!') === 0) {
          if (isset($row['yml'][$key])) {
            return FALSE;
          }
        }
        elseif (!isset($row['yml'][$key])) {
          return FALSE;
        }
        if (!is_array($value)) {
          if (strpos($value, '!') === 0) {
            if ($value !== '!NULL' && $row['yml'][$key] === $value) {
              return FALSE;
            }
          }
          elseif ($row['yml'][$key] !== $value) {
            return FALSE;
          }
        }
        else {
          if (!in_array($row['yml'][$key], $value)) {
            return FALSE;
          }
        }
      }
    }

    $row['processed_content'] = $data->getContent();
    return $this->validateIds($row);
  }

  /**
   * Validate IDs and set them if necessary.
   *
   * May remove row silently, continue using row or raise an exception depending
   * on configuration options.
   *
   * @param array $row
   *   The current row being pre-processed.
   *
   * @return bool
   *   Whether to keep the row or not.
   *
   * @throws \Drupal\migrate\MigrateException
   */
  protected function validateIds(array &$row) {
    foreach ($this->configuration['ids'] as $field => $details) {
      if ($fields = explode('/', $field)) {
        if (isset($row['yml'][$fields[1]])) {
          $value = $row['yml'][$fields[1]];
          if (is_array($value)) {
            $value = array_pop($value);
          }
          $row[$field] = $value;
        }
        if (empty($row[$field])) {
          if (!empty($this->configuration['skip_missing_ids'])) {
            return FALSE;
          }
          if (!empty($this->configuration['throw_error_on_missing_ids'])) {
            throw new MigrateException("Row missing ID field '{$field}'");
          }
        }
      }
    }
    return TRUE;
  }
}
