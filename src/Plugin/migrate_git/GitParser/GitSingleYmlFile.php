<?php

namespace Drupal\migrate_git\Plugin\migrate_git\GitParser;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate_git\GitFetcherPluginManager;
use Drupal\migrate_git\GitParserPluginBase;
use Symfony\Component\Yaml\Yaml;

/**
 * Obtain file contents from single yml files for migration.
 *
 * Available configuration keys:
 *   - flatten: (optional) flatten yml array (rows) based on the flatten key.
 *   - flatten_key: (optional) flatten yml array (rows) with this key.
 *   - flatten_parent_data_src: (optional) provide a parent data source key.
 *
 * Fields vary based on source data.
 *
 * @GitParser(
 *   id = "single_yml_file",
 *   title = @Translation("Git Single Yml File")
 * )
 */
class GitSingleYmlFile extends GitParserPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GitFetcherPluginManager $fetcher_plugin_manager) {
    $configuration['pre_download'] = FALSE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $fetcher_plugin_manager);
  }

  /**
   * {@inheritdoc}
   */
  protected function openSource($repo) {
    /** @var array $info */
    $info = $this->getFetcherPlugin()->getResponse($repo);
    $content = $this->getFetcherPlugin()->downloadFile($info['path'], $info);
    $yml = Yaml::parse($content);
    if (!empty($this->configuration['flatten'])) {
      $data = [];
      foreach ($yml as $key => $item) {
        $parent = $item;
        unset($parent[$this->configuration['flatten_key']]);
        if (empty($parent)) {
          $parent = ['data' => $key];
        }
        else {
          $parent['data'] = $parent[$this->configuration['flatten_parent_data_src']];
        }
        $data[] = $parent;
        foreach ($item[$this->configuration['flatten_key']] as $value) {
          if (!is_array($value)) {
            $value = ['data' => $value];
          }
          $value['parent'] = $parent;
          $data[] = $value;
        }
      }
      $yml = $data;
    }
    foreach ($yml as &$item) {
      $item['file_info'] = $info;
    }
    $this->iterator = new \ArrayIterator($yml);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchNextRow() {
    $current = $this->iterator->current();
    if ($current) {
      $this->currentItem = $current;
      $this->iterator->next();
    }
  }

}
