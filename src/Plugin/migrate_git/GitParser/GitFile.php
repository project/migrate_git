<?php

namespace Drupal\migrate_git\Plugin\migrate_git\GitParser;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate_git\GitParserPluginBase;

/**
 * Obtain file contents for migration from git repo.
 *
 * Available configuration keys:
 *   - pre_download: (optional) Download file contents early in processing.
 *
 * Fields:
 *   - content: file contents.
 *
 * @GitParser(
 *   id = "file",
 *   title = @Translation("File")
 * )
 */
class GitFile extends GitParserPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Iterator over the files.
   *
   * @var \Iterator
   */
  protected $iterator;

  /**
   * {@inheritdoc}
   */
  protected function openSource($source) {
    $source_data = $this->getFetcherPlugin()->getResponse($source);
    foreach ($source_data as $key => &$row) {
      if (!$this->preprocessRow($row)) {
        unset($source_data[$key]);
      }
    }
    $this->iterator = new \ArrayIterator($source_data);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchNextRow() {
    $current = $this->iterator->current();
    if ($current) {
      if (empty($this->configuration['pre_download'])) {
        if ($current['type'] === 'file') {
          $current['content'] = $this->getFetcherPlugin()
            ->downloadFile($current['path'], $current);
        }
      }
      $this->currentItem = $current;
      $this->iterator->next();
    }
  }

  /**
   * Process the row after retrieving the file information from the repository.
   *
   * As well as pre-processing data, perform early removals of rows for accurate
   * counting/lower processing cost.
   *
   * @param array $row
   *   The row to process.
   *
   * @return bool
   *   Whether to keep the row.
   */
  protected function preprocessRow(array &$row) {
    if (!empty($this->configuration['pre_download'])) {
      if ($row['type'] === 'file') {
        $row['content'] = $this->getFetcherPlugin()
          ->downloadFile($row['path'], $row);
      }
    }
    return TRUE;
  }

}
