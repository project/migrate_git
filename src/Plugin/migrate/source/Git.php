<?php

namespace Drupal\migrate_git\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\SourcePluginExtension;

/**
 * Source plugin for retrieving data from git repositories.
 *
 * Available configuration keys:
 *   - fetcher_plugin: What git_fetcher plugin to use.
 *   - parser_plugin: What git_parser plugin to use.
 *   - repos: List of repository/folder combinations to process. Format may vary
 *     depending on fetcher plugin.
 *   - ids: list of ids keys by id path, sub-keyed by 'type' value.
 *
 * See usage examples in GitParser and GitFetcher plugins.
 *
 * @MigrateSource(
 *   id = "git"
 * )
 */
class Git extends SourcePluginExtension {

  /**
   * The git data parser plugin.
   *
   * @var \Drupal\migrate_git\GitParserPluginInterface
   */
  protected $parserPlugin;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    if (!is_array($configuration['repos'])) {
      $configuration['repos'] = [$configuration['repos']];
    }

    // Should handle this better.
    $configuration['fields'] = [];
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * Return a string representing the source repos.
   *
   * @return string
   *   Comma-separated list of repo/paths being imported.
   */
  public function __toString() {
    // This could cause a problem when using a lot of urls, may need to hash.
    return implode(', ', $this->configuration['repos']);
  }

  /**
   * Returns the initialized git parser plugin.
   *
   * @return \Drupal\migrate_git\GitParserPluginInterface
   *   The git parser plugin.
   */
  public function getParserPlugin() {
    if (!isset($this->parserPlugin)) {
      $this->parserPlugin = \Drupal::service('plugin.manager.migrate_git.git_parser')->createInstance($this->configuration['parser_plugin'], $this->configuration);
    }
    return $this->parserPlugin;
  }

  /**
   * Creates and returns a filtered Iterator over the documents.
   *
   * @return \Iterator
   *   An iterator over the documents providing source rows that match the
   *   configured item_selector.
   */
  protected function initializeIterator() {
    return $this->getParserPlugin();
  }

}
