<?php

namespace Drupal\migrate_git\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_git\MarkdownParserPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process jekyll content.
 *
 * Available configuration keys:
 *   - markdown_parser: (optional) Select MarkdownParser plugin to use. Defaults
 *     to parsedown. Any other may require installation of extra PHP Libraries
 *   - skip_fails: (optional) If TRUE, throw a MigrateSkipRowException on any
 *     exceptions/failures of processing the content.
 *   - include_roots: (optional) If provided replace jekyll include statements
 *     with the include content with path provided for the syntax.
 *   - variables: (optional) If provided replace jekyll render commands with
 *     the provided variable results.
 *   - replace: (optional) If provided, perform simple string replacements with
 *     the provided search/replace pairs.
 *   - replace_regex: (optional) If provided perform regexp based string
 *     replacements.
 *
 * Example:
 *
 * @code
 * process:
 *   'body/value':
 *     plugin: jekyll_content
 *     source: processed_content
 *     markdown_parser: php_markdown_extra
 *     variables:
 *       site:
 *         url: ''
 *         baseurl: ''
 *     skip_fails: true
 *     replace:
 *       '{:.execute_result}': ''
 *       '{:.input}': ''
 *       '(/images/': '(/sites/default/files/images/'
 *     replace_regex:
 *       '/{:data-proofer-ignore .*?}/': ''
 *       '/{% include toc .*? %}/': ''
 *     include_roots:
 *       'include/': '_includes/'
 *       'include /': '_includes/'
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "jekyll_content",
 * )
 */
class JekyllContent extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The git fetcher plugin.
   *
   * @var \Drupal\migrate_git\GitFetcherPluginInterface
   */
  protected $gitFetcher;

  /**
   * A markdown parser plugin to parse includes with.
   *
   * @var \Drupal\migrate_git\MarkdownParserPluginInterface
   */
  protected $markdownParser;

  /**
   * Constructs a MigrationLookup object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The Migration the plugin is being used in.
   * @param \Drupal\migrate_git\MarkdownParserPluginManager $markdown_plugin_manager
   *   The markdown plugin manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, MarkdownParserPluginManager $markdown_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    /** @var \Drupal\migrate_git\Plugin\migrate\source\Git $source */
    $source = $migration->getSourcePlugin();
    $this->gitFetcher = $source->getParserPlugin()->getFetcherPlugin();
    $parser = $this->configuration['markdown_parser'] ?? 'parsedown';
    $this->markdownParser = $markdown_plugin_manager->createInstance($parser, $this->configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('plugin.manager.migrate_git.markdown_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $config = $this->configuration;

    // Import included files.
    $matches = [];
    $replace = [];
    preg_match_all('/{%(.*?)%}/', $value, $matches);
    if (!empty($matches[1])) {
      foreach ($matches[1] as $i => $match) {
        try {
          if (strpos($match, 'include') !== FALSE) {
            if (isset($config['include_roots'])) {
              $match = trim(str_ireplace(
                array_keys($config['include_roots']),
                array_values($config['include_roots']),
                $match));
            }
            $listing = $this->gitFetcher->listFiles($match, FALSE);
            $document = $this->markdownParser->parse(
              $this->gitFetcher->downloadFile($match, $listing)
            );
            $text = $this->transform($document, $migrate_executable, $row, $destination_property);
            $replace[$matches[0][$i]] = $text;
          }
          // Any non-includes we need to process?
        }
        catch (\Exception $e) {
          if (!empty($this->configuration['skip_fails'])) {
            continue;
          }
          throw new MigrateSkipRowException("Jenkins processing failed: {$e->getMessage()}");
        }
      }
      $value = str_ireplace(array_keys($replace), array_values($replace), $value);
    }

    // Replace variables.
    $vars = $this->configuration['variables'] ?? [];
    $vars['page'] = $row->getSourceProperty('yml');
    $matches = [];
    $replace = [];
    preg_match_all('/{{(.*?)}}/', $value, $matches);
    if (!empty($matches[1])) {
      foreach ($matches[1] as $i => $match) {
        try {
          $keys = explode('.', trim($match));
          if (isset($keys[1])) {
            $replace[$matches[0][$i]] = $vars[$keys[0]][$keys[1]];
          }
          else {
            $replace[$matches[0][$i]] = $vars[$keys[0]];
          }
        }
        catch (\Exception $e) {
          if (!empty($this->configuration['skip_fails'])) {
            continue;
          }
          throw new MigrateSkipRowException("Jenkins processing failed: {$e->getMessage()}");
        }
      }
      $value = str_ireplace(array_keys($replace), array_values($replace), $value);
    }

    // Perform regex based replacements if defined in process config.
    if (isset($config['replace_regex'])) {
      $value = preg_replace(
        array_keys($config['replace_regex']),
        array_values($config['replace_regex']),
        $value);
    }

    // Perform simple replace operations if defined in process config.
    if (isset($config['replace'])) {
      $value = str_ireplace(
        array_keys($config['replace']),
        array_values($config['replace']),
        $value);
    }

    // Remove left over empty <p> tags.
    return str_replace(
      [
        '<p></p>',
        '<p> </p>',
        '<p>&nbsp;</p>',
      ],
      '',
      $value);
  }

}
