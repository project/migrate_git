<?php

namespace Drupal\migrate_git;

/**
 * Defines an interface for git data fetchers.
 *
 * @see \Drupal\migrate_git\Annotation\GitFetcher
 * @see \Drupal\migrate_git\GitFetcherPluginBase
 * @see \Drupal\migrate_git\GitFetcherPluginManager
 * @see plugin_api
 */
interface GitFetcherPluginInterface {

  /**
   * Return content.
   *
   * @param string $repo
   *   The repository url to retrieve from. Different fetcher plugins may have
   *   different formats so make sure to read the documentation for the fetcher.
   *
   * @return array
   *   File information from the repo/path.
   */
  public function getResponse($repo);

  /**
   * List files and directories in provided path, recursively by default.
   *
   * @param string $path
   *   The path to list files.
   * @param bool $recursive
   *   Whether to recurse into sub-folders. Default: TRUE.
   *
   * @return array
   *   Array of file information arrays.
   */
  public function listFiles($path, $recursive = TRUE);

  /**
   * Download file contents from a specific file in the git repository.
   *
   * @param string $path
   *   The path to the file to download.
   * @param array $row
   *   The full file details row.
   *
   * @return string
   *   Contents of the file.
   */
  public function downloadFile($path, array $row);

}
