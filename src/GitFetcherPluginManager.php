<?php

namespace Drupal\migrate_git;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a plugin manager for data fetchers.
 *
 * @see \Drupal\migrate_git\Annotation\GitFetcher
 * @see \Drupal\migrate_git\GitFetcherPluginBase
 * @see \Drupal\migrate_git\GitFetcherPluginInterface
 * @see plugin_api
 */
class GitFetcherPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new GitFetcherPluginManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/migrate_git/GitFetcher',
      $namespaces,
      $module_handler,
      'Drupal\migrate_git\GitFetcherPluginInterface',
      'Drupal\migrate_git\Annotation\GitFetcher'
    );

    $this->alterInfo('git_fetcher_info');
  }

}
