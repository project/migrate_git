<?php

namespace Drupal\migrate_git;

use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base data parser implementation.
 *
 * @see \Drupal\migrate_git\Annotation\DataParser
 * @see \Drupal\migrate_git\GitParserPluginInterface
 * @see \Drupal\migrate_git\GitParserPluginManager
 * @see plugin_api
 */
abstract class GitParserPluginBase extends PluginBase implements GitParserPluginInterface {

  /**
   * List of source repos and paths.
   *
   * @var string[]
   */
  protected $repos;

  /**
   * Index of the currently-open repo.
   *
   * @var int
   */
  protected $activeRepo;

  /**
   * Current item when iterating.
   *
   * @var mixed
   */
  protected $currentItem = NULL;

  /**
   * Value of the ID for the current item when iterating.
   *
   * @var string
   */
  protected $currentId = NULL;

  /**
   * The git data retrieval client.
   *
   * @var \Drupal\migrate_git\GitFetcherPluginInterface
   */
  protected $fetcher;

  /**
   * The git fetcher plugin manager.
   *
   * @var \Drupal\migrate_git\GitFetcherPluginManager
   */
  protected $fetcherManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GitFetcherPluginManager $fetcher_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->repos = $configuration['repos'];
    $this->fetcherManager = $fetcher_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.migrate_git.git_fetcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFetcherPlugin() {
    if (!isset($this->fetcher)) {
      $this->fetcher = $this->fetcherManager->createInstance($this->configuration['fetcher_plugin'], $this->configuration);
    }
    return $this->fetcher;
  }

  /**
   * {@inheritdoc}
   */
  public function rewind() {
    $this->activeRepo = NULL;
    $this->next();
  }

  /**
   * Implementation of Iterator::next().
   */
  public function next() {
    $this->currentItem = $this->currentId = NULL;
    if (is_null($this->activeRepo)) {
      if (!$this->nextSource()) {
        // No data to import.
        return;
      }
    }
    // At this point, we have a valid open source url, try to fetch a row from
    // it.
    $this->fetchNextRow();
    // If there was no valid row there, try the next url (if any).
    if (is_null($this->currentItem)) {
      while ($this->nextSource()) {
        $this->fetchNextRow();
        if ($this->valid()) {
          break;
        }
      }
    }
    if ($this->valid()) {
      foreach ($this->configuration['ids'] as $id_field_name => $id_info) {
        $this->currentId[$id_field_name] = $this->currentItem[$id_field_name];
      }
    }
  }

  /**
   * Advances the git parser to the next source.
   *
   * @return bool
   *   TRUE if a valid source URL was opened
   */
  protected function nextSource() {
    while ($this->activeRepo === NULL || (count($this->repos) - 1) > $this->activeRepo) {
      if (is_null($this->activeRepo)) {
        $this->activeRepo = 0;
      }
      else {
        // Increment the activeRepo so we try to load the next source.
        $this->activeRepo++;
        if ($this->activeRepo >= count($this->repos)) {
          return FALSE;
        }
      }

      if ($this->openSource($this->repos[$this->activeRepo])) {
        // We have a valid source.
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function current() {
    return $this->currentItem;
  }

  /**
   * {@inheritdoc}
   */
  public function key() {
    return $this->currentId;
  }

  /**
   * {@inheritdoc}
   */
  public function valid() {
    return !empty($this->currentItem);
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    $count = 0;
    foreach ($this as $item) {
      $count++;
    }
    return $count;
  }

  /**
   * Opens the specified git repo/path.
   *
   * @param string $source
   *   Repo/path to open.
   *
   * @return bool
   *   TRUE if the source was successfully opened, FALSE otherwise.
   */
  abstract protected function openSource($source);

  /**
   * Retrieves the next row of data. populating currentItem.
   *
   * Retrieves from the open source URL.
   */
  abstract protected function fetchNextRow();

}
