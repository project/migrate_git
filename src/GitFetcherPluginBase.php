<?php

namespace Drupal\migrate_git;

use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base git fetcher implementation.
 *
 * @see \Drupal\migrate_git\Annotation\GitFetcher
 * @see \Drupal\migrate_git\GitFetcherPluginInterface
 * @see \Drupal\migrate_git\GitFetcherPluginManager
 * @see plugin_api
 */
abstract class GitFetcherPluginBase extends PluginBase implements GitFetcherPluginInterface {

  /**
   * The git user/organization id.
   *
   * @var string
   */
  protected $user;

  /**
   * The repository id.
   *
   * @var string
   */
  protected $repo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

}
