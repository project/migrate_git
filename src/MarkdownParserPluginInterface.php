<?php

namespace Drupal\migrate_git;

use Mni\FrontYAML\Markdown\MarkdownParser;

/**
 * Defines an interface for markdown parsers.
 *
 * @see \Drupal\migrate_git\Annotation\MarkdownParser
 * @see \Drupal\migrate_git\MarkdownParserPluginManager
 * @see plugin_api
 */
interface MarkdownParserPluginInterface extends MarkdownParser {

  /**
   * Parses Markdown to generate HTML.
   *
   * @param string $input
   *   Markdown input to transform.
   *
   * @return string
   *   The generated HTML.
   */
  public function parse($input);

}
