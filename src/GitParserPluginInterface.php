<?php

namespace Drupal\migrate_git;

/**
 * Defines an interface for data parsers.
 *
 * @see \Drupal\migrate_git\Annotation\GitParser
 * @see \Drupal\migrate_git\GitParserPluginBase
 * @see \Drupal\migrate_git\GitParserPluginManager
 * @see plugin_api
 */
interface GitParserPluginInterface extends \Iterator, \Countable {

  /**
   * Returns the initialized git fetcher plugin.
   *
   * @return \Drupal\migrate_git\GitFetcherPluginInterface
   *   The git fetcher plugin.
   */
  public function getFetcherPlugin();

}
