<?php

namespace Drupal\migrate_git\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a git parser annotation object.
 *
 * Plugin namespace: Plugin\migrate_git\GitParser.
 *
 * @see \Drupal\migrate_git\GitParserPluginBase
 * @see \Drupal\migrate_git\GitParserPluginInterface
 * @see \Drupal\migrate_git\GitParserPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class GitParser extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The title of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

}
