<?php

namespace Drupal\migrate_git\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an markdown annotation object.
 *
 * Plugin namespace: Plugin\migrate_git\MarkdownParser.
 *
 * @see \Drupal\migrate_git\MarkdownParserPluginInterface
 * @see \Drupal\migrate_git\MarkdownParserPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class MarkdownParser extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The title of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

}
