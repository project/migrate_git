<?php

namespace Drupal\migrate_git\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a data fetcher annotation object.
 *
 * Plugin namespace: Plugin\migrate_git\GitFetcher.
 *
 * @see \Drupal\migrate_git\GitFetcherPluginBase
 * @see \Drupal\migrate_git\GitFetcherPluginInterface
 * @see \Drupal\migrate_git\GitFetcherPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class GitFetcher extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The title of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

}
