The migrate_git module provides a git source plugin with various other plugins
to effectively migrate data from git to Drupal.

## Usage
Provides no front end functionality this is entirely for usage within migration
definitions.

See doc-blocks of the specific plugins for details on their usage.

# Installation
Must be installed via Composer - requires multiple external PHP libraries.

## Plugin types
migrate_git provides the following plugin types:

* A `git_parser` type, for parsing different formats on behalf of the `git`
  source plugin.
* A `git_fetcher` type, for fetching data to feed into a `git_parser` plugin.
* A `markdown_parser` type, for parsing markdown content into HTML. Used by
  various `git_parser` plugins as well as the `jekyll_content` process plugin.

## Plugins

### Process

* `jekyll_content`: provides parsing functionality for jekyll style markdown.

### Source

* `git`: A source plugin relying on a combination of `git_parser` and 
  `git_fetcher` plugins to import data from a git repository.
  
### Git Fetcher

* `github`: Fetches data from a github based git repository.

### Git Parser

* `file`: Parses basic file contents for consumption.
* `frontyml`: Parses yml based frontmatter files for consumption.
* `single_yml_file`: Parses single yml files for rows of yml to consume.

### Markdown Parser
Various markdown files are subtly different so these normalize interfaces to
different markdown parsing libraries so that Git Parser plugins or the
`jekyll_content` plugin can properly parse your markdown source.

* `parsedown`: Default parser if no alternative is selected.
* `cebe_github`: Requires external PHP library `cebe/markdown`.
* `cebe_markdown`: Requires external PHP library `cebe/markdown`.
* `cebe_markdown_extra`: Requires external PHP library `cebe/markdown`.
* `commonmark`: Requires external PHP library `league/commonmark`.
* `php_markdown` Requires external PHP library `michelf/markdown`.
* `php_markdown_extra` Requires external PHP library `michelf/markdown`.


